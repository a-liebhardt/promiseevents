/*
 * Register an event listener
 *
 * @param   string    type
 * @param   callback  function
 * @return  booelan
 */
module.exports.EventOn = function(type, callback) {
  if (!process._events || !process._events) {
    console.warn('Could not read process event list!');
    console.error('Event registration stopped.');
    return false;

  }

  if (process._events[type]) {
    console.warn(`Event '${type}' is already registered.`);
    return false;

  }

  process.on(type, (data) => {
    // Exec processing callback
    callback(data.config).then(data.callback).catch(data.callback);
  });

  return true;

};

/*
 * Emit an registered event listener
 *
 * @param   string    type
 * @param   config    object
 * @return  boolean|promise
 */
module.exports.EventEmit = function(type, config) {
  if (!process._events || !process._events) {
    console.warn('Could not read process event list!');
    console.error('Event registration stopped.');
    return false;

  }

  return new Promise(async (resolve, reject) => {
    const maxTries = 4;
    const checkInMs = 500;

    // Register event callback
    // That's the bridge between the event and the processing callback
    const callback = (success, error) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(success);
    };

    // Sub function to better handle a not found situation
    const emitEvent = (i) => {
      i = i ? i : 0;
      if (!process._events[type]) {
        i++;
        if (i > maxTries) {
          reject(new Error(`Event '${type}' not found.`));
          return;
        }

        console.info(`Event '${type}' not found. Will try again ...`);
        setTimeout(emitEvent.bind(this, i), checkInMs);
        return;
        
      }

      // Emit with event callback
      process.emit(type, {
        config: config ? config : {},
        callback: callback,
      });
    }

    emitEvent();
  });
};