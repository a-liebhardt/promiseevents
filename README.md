# PromiseEvents

Allow the use of promise states across default nodejs events.

## Demo

Run ` node example ` to see a demonstration. 

## Usage

The module provide two functions to register and execute events.

### ` EventOn ` - Register a new event

Update your project modules, register a new event listener by name and callback function at the initialisation phase.

To get proper event names best practise is to use ` <Class name>.<Function name> `.

> Important: The choosen event name must be unique!

``` 
File: foo.js

import { EventOn } from 'promiseevents';

export default class Foo {
  constructor {
    EventOn('Foo.anyFunc', this.anyFunc.bind(this));
    ...
  }
  ...
  async anyFunc(config) {
    ...
  }
  ...
}

```

Callback func is expected to return a promise.

```
async anyFunc(config) {
  ...
}

anyFunc(config) {
  return new Promise((resolve, reject) => {
    ...
  };
}
```

### ` EventEmit ` - Execute an registered event

Import Foo into your project. An import at root level works best. 

```
File: index.js

import 'foo';
import Far from 'far'
Far.anyController();
```

Now the registered events can be used everywhere in your project. 

``` 
File: far.js

import { EventEmit } from 'promiseevents';

class Far {

  ...

  async anyController() {
    EventEmit('Foo.anyFunc', { msg: "Hello World" })
      .then(config => {
        return EventEmit('Foo.anyNextFunc', config);
      })
      .then(config => EventEmit.bind(this, 'Bar.anyOtherFunc', config))
      .then(config => console.log)
      .catch(console.err);
  }

  ...
}
``` 

> Because it is a loose binding there is no need to import ` Foo ` anywhere else in the project like it is done usually in an traditional flow. 

## Error handling

### ` TypeError: callback(...).then is not a function `

Make sure you use a async processing function like it is shown in the example ` anyFunc `.

## License
MIT

