const { EventEmit, EventOn } = require('./index');

EventOn('Example.demo', async function(config) {
  console.log('Example.demo was executed');
  config.with = 'Friends'; 
  return config;
});

EventEmit('Example.demo', { hello: 'World' }).then(console.log).catch(console.error);